var gulp 	= require('gulp'),
	sass 	= require('gulp-sass'),
	rename 	= require('gulp-rename'),
	prefix 	= require('gulp-autoprefixer'),
	bs 		= require('browser-sync'),
	uglify	= require('gulp-uglify'),

	svgstore = require('gulp-svgstore'),
	svg2string = require('gulp-svg2string'),
	svgmin = require('gulp-svgmin');

gulp.task('svg', function () {
  return gulp.src('src/img/svg/*.svg')
	.pipe(svgmin())
    .pipe(svgstore())
    .pipe(svg2string())
    .pipe(gulp.dest('src/img'));
});

// CORE OPERATIONS
gulp.task('sass', function(){
	gulp.src('src/scss/all-styles.scss')
		.pipe(sass({
			outputStyle: 'compressed',
			includePaths: ['./src/scss/', './src/scss/styles/', './src/modules/'],
		})
			.on('error', sass.logError))
		.pipe(prefix())
		.pipe(rename('s.min.css'))
		.pipe(gulp.dest('src/css'))
		.pipe(bs.stream());
});


var reloadTimer;
function reloadDelay(){
	clearTimeout(reloadTimer);
	reloadTimer = setTimeout(bs.reload, 1000);
}

gulp.task('svg-watch', ['svg'], reloadTimer);

//gulp.task('imagemin', function() {
//    return gulp.src(['src/**/*.png', 'src/**/*.jpg'])
//        .pipe(imagemin({
//            progressive: true,
//        }))
//        .pipe(gulp.dest('src/'));
//});
//
//gulp.task('jsmin', function() {
//	gulp.src(['src/js/**/*.js', '!src/js/**/*.min.js'])
//	  .pipe(uglify())
//	  .pipe(rename({
//	  	suffix: '.min'
//	  }))
//	  .pipe(gulp.dest('src/javascripts/'));
//});

// END OF CORE OPERATIONS

// WRAPPING UP: initial compile and start watchers
gulp.task('default', ['svg', 'sass'], function(){
	bs({
		proxy: 'localhost:8888/2017-10-01 - Small & Middle Business/front/src'
	});

	gulp.watch(['src/scss/*.scss',
							'src/scss/**/*.scss',
							'src/modules/**/*.scss'], ['sass']);

	gulp.watch(['src/*.php',
	            'src/*.js',
	            'src/*.jpg',
	            'src/*.png',

	            'src/**/*.php',
	            'src/**/*.js',
	            'src/**/*.jpg',
	            'src/**/*.png',

	            'src/fonts/**/*',
	            'src/fonts/*'], 
	            bs.reload);


	gulp.watch(['src/img/svg/*.svg'], ['svg-watch']);

});
