<? include('modules/head.php'); ?>
<h1 style="margin: 0px 0 0 0">Верстка "Малый и средний бизнес"</h1>
<ul style="margin: 10px 0 0 10px">
  <li><a href="main.php">Главная страница</a></li>
  <li><a href="invest.php">Инвестиции - список</a></li>
  <li><a href="invest-one.php">Инвестиции - детальная</a></li>
  <li><a href="cities.php">Города - список</a></li>
  <li><a href="consult.php">Консультации - список</a></li>
  <li><a href="consult-one.php">Консультации - детальная</a></li>
  <li><a href="lk.php">Личный кабинет (вход/регистрация)</a></li>
  <li><a href="search.php">Поиск</a></li>
  <li><a href="search-result.php">Поиск - результаты</a></li>
  <li><a href="news.php">Новости - список</a></li>
  <li><a href="news-one.php">Новости - детальная</a></li>
</ul>