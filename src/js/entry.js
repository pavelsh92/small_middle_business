if (documentReady) onDocReady();
else $doc.one('ready', onDocReady);

if (windowLoaded) onWindowLoad();
else $win.one('load', onWindowLoad);