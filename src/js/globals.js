var $body, $doc = $(document), $win = $(window),
	windowLoaded = false, documentReady = false,
	resizeHandlers = [], resizeTimeout;

// process all "on resize" functions in one place with delay to ensure
// that resizing is ended.
// How to use: every time you need to do something on resize,
// make this "something" a named function, and then add it like this:
// resizeHandlers.push(something);
// And that's it. You're done.
function resizeDelay(){
	if (resizeHandlers.length === 0) return;
	clearTimeout(resizeTimeout);
	resizeTimeout = setTimeout(function(){
		for (var i = 0; i < resizeHandlers.length; i++){
			if (typeof resizeHandlers[i] === 'function'){
				resizeHandlers[i].call();
			}
		}
	}, 300); 
}
$win.on('resize', resizeDelay);

$win.one('load', function(){
	windowLoaded = true;
});

$doc.one('ready', function(){
	$body = $('body');
	documentReady = true;
});