<nav class="breadcrumb">
  <a class="breadcrumb-item" href="#">Главная</a>
  <a class="breadcrumb-item" href="#">Инфраструктура поддержки</a>
  <span class="breadcrumb-item active">Городские округа</span>
</nav>

<h1>Городские округа</h1>

<div class="ps-container">
<ul class="cities-nav list-unstyled">
  <li><a href="#cities-a">а</a></li>
  <li><a href="#cities-b">б</a></li>
  <li><a href="#cities-d">д</a></li>
  <li><a href="#cities-e">е</a></li>
  <li><a href="#cities-zh">ж</a></li>
  <li><a href="#cities-z">з</a></li>
  <li><a href="#cities-i">и</a></li>
  <li><a href="#cities-k">к</a></li>
  <li><a href="#cities-l">л</a></li>
  <li><a href="#cities-m">м</a></li>
  <li><a href="#cities-n">н</a></li>
  <li><a href="#cities-s">с</a></li>
  <li><a href="#cities-sh">ш</a></li>
</ul>
  <ul class="cities">
    <li class="cities__item" id="cities-a">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Ачинск
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item" id="cities-b">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Боготол
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Бородино
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item" id="cities-d">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Дивногорск
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item" id="cities-e">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Енисейск
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item" id="cities-zh">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Железногорск
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item" id="cities-z">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Заозерный
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Зеленогорск
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item" id="cities-i">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Игарка
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item" id="cities-k">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Канск
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Кедровый
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Красноярск
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item" id="cities-l">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Лесосибирск
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item" id="cities-m">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Минусинск
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item"  id="cities-n">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Назарово
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Норильск
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item" id="cities-s">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Солнечный ЗАТО
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Сосновоборск
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
    <li class="cities__item" id="cities-sh">
      <div class="cities__name">
        <div class="svg-wrap"><svg><use xlink:href="#list"></use></svg></div>
        Шарыпово
        <div class="btn-collapse"></div>
      </div>
      <? include('modules/page-cities/cities__description.php') ?>
    </li>
  </ul>
</div>