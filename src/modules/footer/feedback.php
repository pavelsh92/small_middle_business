<section class="feedback">
  <div class="feedback__title">Обратная связь</div>
  <div class="feedback__wrap">
    <div class="feedback__form">
      <form>
        <div class="title">Ваши пожелания</div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="name">Имя: <span class="star-required">*</span></label>
            <input type="name" class="form-control" id="name">
          </div>
          <div class="form-group col-md-6">
            <label for="email-tel">E-mail / тел.: <span class="star-required">*</span></label>
            <input type="email" class="form-control" id="email-tel">
          </div>
        </div>
        <div class="form-group">
          <label for="question">Содержание вопроса: <span class="star-required">*</span></label>
          <textarea name="question" class="form-control"  id="question"></textarea>
        </div>
        <div class="validate">
          <div class="text">Проверка</div>
          <div class="g-recaptcha" data-sitekey="0000000000000000000000"></div>
        </div>
        <div class="form-footer">
          <small>Поля, отмеченные <span class="star-required">*</span>, обязательны для заполнения</small>
          <button type="submit" class="btn btn-1 red">Отправить<div class="svg-wrap">
            <svg>
              <use xlink:href="#arrow1"></use>
            </svg>
          </div></button>
        </div>
      </form>
    </div>
    <div class="feedback__contacts">
      <div class="contact">
        <div class="content">Отдел предпринимательства министерства экономического развития и инвестиционной политики Красноярского края</div>
      </div>
      <div class="contact">
        <div class="title">Адрес</div>
        <div class="content">ул.Ленина, 125,<br>Красноярск, 660000.</div>
      </div>
      <div class="contact">
        <div class="title">Телефон</div>
        <div class="content">(391) 211-23-39</div>
      </div>
    </div>
  </div>
</section>