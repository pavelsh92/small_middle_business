<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PS Template BS4</title>
  <link rel="stylesheet" href="css/s.min.css">
  <script src="https://code.jquery.com/jquery.js"></script> 
  <script src="img/svg.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

  <!-- SVG sprite include -->
  <div class="svg-placeholder"
    style="border: 0; clip: rect(0 0 0 0); height: 1px;
      margin: -1px; overflow: hidden; padding: 0;
      position: absolute; width: 1px;"></div>
  <script>
    document.querySelector('.svg-placeholder').innerHTML = SVG_SPRITE;
  </script> 
  <!-- end SVG sprite include -->
<div class="main-wrap"> 

<? include('modules/header/header.php'); ?>