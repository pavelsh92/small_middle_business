<nav class="breadcrumb">
  <a class="breadcrumb-item" href="#">Главная</a>
  <span class="breadcrumb-item active">Новости</span>
</nav>

<h1>Новости</h1>

<section class="news">
    <form class="news__filter">
      <div class="form-group">
        <label for="month">Месяц:</label>
        <select class="form-control no-search" id="month">
          <option value="">Январь</option>
          <option value="">Февраль</option>
          <option value="">Март</option>
          <option value="">Апрель</option>
          <option value="">Май</option>
          <option value="">Июнь</option>
          <option value="">Июль</option>
          <option value="">Август</option>
          <option value="">Сентябрь</option>
          <option value="">Октябрь</option>
          <option value="">Ноябрь</option>
          <option value="">Декабрь</option>
        </select>
      </div>
      <div class="form-group">
        <label for="year">Год:</label>
        <select class="form-control  no-search" id="year">
          <option value="">2017</option>
          <option value="">2016</option>
          <option value="">2015</option>
          <option value="">2014</option>
          <option value="">2013</option>
          <option value="">2012</option>
          <option value="">2011</option>
          <option value="">2010</option>
          <option value="">2009</option>
          <option value="">2008</option>
          <option value="">2007</option>
          </select>
      </div>
      <div class="form-group">
        <label for="category">Категория:</label>
        <select class="form-control" id="category">
          <option value="">Анонсы</option>
          <option value="">Новости</option>
          <option value="">Объявления</option>
          <option value="">Мероприятия</option>
        </select>
      </div>
      <button type="submit" class="btn btn-1 red">Показать<div class="svg-wrap">
        <svg>
          <use xlink:href="#arrow1"></use>
        </svg>
      </div></button>
    </form>
  <div class="news__list">
    <div class="news__item">
      <div class="date">18.07.2017 / Новости</div>
      <a href="news-one.php" class="link-mark-all name"><span class="text">Верните жалобную книгу</span></a>
      <a href="news-one.php" class="link red">Узнать детали</a>
    </div>
    <div class="news__item news__item-big">
      <a href="news-one.php" class="img link-reset"><img src="modules/page-news/img/news-1.jpg" alt=""></a>
      <div class="date">18.07.2017 / Мероприятия</div>
      <a href="news-one.php" class="link-mark-all name"><span class="text">Предпринимателей приглашают на семинар по госзакупкам</span></a>
      <div class="desc">Семинар «Современная система закупок. Правила работы на электронных торговых площадках: от регистрации до контракта, новые возможности» пройдёт 21 июля 2017 года с 12:00 до 16:00 часов по адресу: г. Красноярск, ул. Новосибирская, 9 А, 2 этаж (Агентство развития бизнеса). Семинар рассчитан на предпринимателей, специалистов, руководителей предприятий и компаний, принимающих участие в электронных торгах. Участие в семинаре бесплатное.</div>
      <a href="news-one.php" class="link red">Узнать детали</a>
    </div>
    <div class="news__item">
      <div class="date">18.07.2017 / Новости</div>
      <a href="news-one.php" class="link-mark-all name"><span class="text">Собирается ли Роспотребнадзор закрыть крупнейшие торговые сети</span></a>
      <a href="news-one.php" class="link red">Узнать детали</a>
    </div>
    <div class="news__item">
      <div class="date">18.07.2017 / Новости</div>
      <a href="news-one.php" class="link-mark-all name"><span class="text">Верховный суд разъяснил, сколько есть времени у уволенного сотрудника для спора с работодателем</span></a>
      <a href="news-one.php" class="link red">Узнать детали</a>
    </div>
    <div class="news__item">
      <div class="date">18.07.2017 / Новости</div>
      <a href="news-one.php" class="link-mark-all name"><span class="text">Налоговиков ограничили в доначислении платежей</span></a>
      <a href="news-one.php" class="link red">Узнать детали</a>
    </div>
    <div class="news__item">
      <div class="date">18.07.2017 / Новости</div>
      <a href="news-one.php" class="link-mark-all name"><span class="text">Аэроэкспресс доставит красноярцев в аэропорт за 49 минут</span></a>
      <a href="news-one.php" class="link red">Узнать детали</a>
    </div>
    <div class="news__item news__item-big">
      <a href="news-one.php"  class="img link-reset"><img src="modules/page-news/img/news-2.jpg" alt=""></a>
      <div class="date">18.07.2017 / Мероприятия</div>
      <a href="news-one.php" class="link-mark-all name"><span class="text">Предпринимателей приглашают на семинар «Провокатор - Манипулятор»</span></a>
      <div class="desc">Семинар пройдёт 27 июля 2017 года с 10:00 до 15:00 часов по адресу: г. Красноярск, ул. Новосибирская, 9 А, 2 этаж (Агентство развития бизнеса). Участие в семинаре бесплатное. Семинар рассчитан на собственников и руководителей компаний малого и среднего бизнеса, специалистов по продажам, по управлению персоналом, по безопасности.</div>
      <a href="news-one.php" class="link red">Узнать детали</a>
    </div>
    <div class="news__item">
      <div class="date">18.07.2017 / Объявления</div>
      <a href="news-one.php" class="link-mark-all name"><span class="text">Красноярская «Медовая компания» (бренд «Дары Сибири») стала официальным лицензиатом Универсиады 2019</span></a>
      <a href="news-one.php" class="link red">Узнать детали</a>
    </div>
    <div class="news__item">
      <div class="date">18.07.2017 / Новости</div>
      <a href="news-one.php" class="link-mark-all name"><span class="text">На инновационные проекты в Красноярском крае направят 215 млн рублей</span></a>
      <a href="news-one.php" class="link red">Узнать детали</a>
    </div>
    <div class="news__item">
      <div class="date">18.07.2017 / Новости</div>
      <a href="news-one.php" class="link-mark-all name"><span class="text">Прожиточному минимуму добавят 420 рублей</span></a>
      <a href="news-one.php" class="link red">Узнать детали</a>
    </div>
  </div>
</section>


<nav aria-label="Page navigation">
  <ul class="pagination">
    <li class="page-item disabled"><a class="page-link" href=""><span class="svg-wrap"><svg><use xlink:href="#pagination-arrow"></use></svg> </span> </a></li>
    <li class="page-item active"><a class="page-link" href="">1</a></li>
    <li class="page-item"><a class="page-link" href="">2</a></li>
    <li class="page-item"><a class="page-link" href="">3</a></li>
    <li class="page-item"><a class="page-link" href="">4</a></li>
    <li class="page-item"><a class="page-link" href="">5</a></li>
    <li class="page-item"><a class="page-link" href="">6</a></li>
    <li class="page-item"><a class="page-link" href="">7</a></li>
    <li class="page-item"><a class="page-link" href=""><span class="svg-wrap"><svg><use xlink:href="#pagination-arrow"></use></svg> </span> </a></li>
  </ul>
</nav>